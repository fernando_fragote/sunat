'use strict'

const builder = require('xmlbuilder');
const fs = require('fs');
const $ = require('underscore')
const base64 = require('base-64')
const forge = require('node-forge')
const SignedXml = require('xml-crypto').SignedXml

const rutaCertificado = 'src/firma/certificado.pem'
const rutaCertificadoPublico = 'src/firma/pubcert.pem'

var pki = forge.pki;

var UBL_EXTENSION_NAMESPACE = ( 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2' );
var XPATH_TO_SIGNATURE_CONTAINER = (
'//*[local-name(.)="UBLExtensions" and ' +
'namespace-uri(.)="' + UBL_EXTENSION_NAMESPACE + '"]' +
'/*[local-name(.)="UBLExtension" and ' +
'namespace-uri(.)="' + UBL_EXTENSION_NAMESPACE + '"][last()]' +
'/*[local-name(.)="ExtensionContent" and ' +
'namespace-uri(.)="' + UBL_EXTENSION_NAMESPACE + '"][last()]'
);


function KeyInfoProvider(certificatePEM) {
  if (!this instanceof KeyInfoProvider) {
    return new KeyInfoProvider();
  }

  if (Buffer.isBuffer(certificatePEM)) {
    certificatePEM = certificatePEM.toString('ascii');
  }

  if (certificatePEM == null || typeof certificatePEM !== 'string') {
    throw new Error('certificatePEM must be a valid certificate in PEM format');
  }

  this._certificatePEM = certificatePEM;

  this.getKeyInfo = function(key, prefix) {
    var keyInfoXml,
      certObj,
      certBodyInB64;

    prefix = prefix || '';
    prefix = prefix ? prefix + ':' : prefix;

    certBodyInB64 = forge.util.encode64(forge.pem.decode(this._certificatePEM)[0].body);
    certObj = pki.certificateFromPem(this._certificatePEM);

    keyInfoXml = '<' + prefix + 'X509Data>';

    keyInfoXml += '<' + prefix + 'X509SubjectName>';
    keyInfoXml += getSubjectName(certObj);
    keyInfoXml += '</' + prefix + 'X509SubjectName>';

    keyInfoXml += '<' + prefix + 'X509Certificate>';
    keyInfoXml += certBodyInB64;
    keyInfoXml += '</' + prefix + 'X509Certificate>';

    keyInfoXml += '</' + prefix + 'X509Data>';

    return keyInfoXml;
  };

  this.getKey = function() {
    return this._certificatePEM;
  };
}

function getSubjectName(certObj) {
  var subjectFields,
    fields = ['CN', 'OU', 'O', 'L', 'ST', 'C'];

  if (certObj.subject) {
    subjectFields = fields.reduce(function(subjects, fieldName) {
      var certAttr = certObj.subject.getField(fieldName);

      if (certAttr) {
        subjects.push(fieldName + '=' + certAttr.value);
      }

      return subjects;
    }, []);
  }

  return Array.isArray(subjectFields) ? subjectFields.join(',') : '';
}

function enviarSunat(documento){
    console.log("enviando documento " + documento.name + " a SUNAT");
}

function crearArchivo(content,name){
    fs.writeFile("./tmp/"+name+".xml", content, function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("Documento "+name+".xml creado satisfactoriamente!");
    });
}

function firmarDocumento(documento, idSignature){

    var sig = new SignedXml();

    sig.addReference("/*", 
    ['http://www.w3.org/2000/09/xmldsig#enveloped-signature',
     'http://www.w3.org/2001/10/xml-exc-c14n#'], 
     'http://www.w3.org/2000/09/xmldsig#sha1', '', '', '', true);

    sig.keyInfoProvider = new KeyInfoProvider(fs.readFileSync(rutaCertificadoPublico))

    sig.signingKey = fs.readFileSync(rutaCertificado)
    
    sig.computeSignature(documento.end({ pretty: true}),{
      prefix: 'ds', location: { reference: XPATH_TO_SIGNATURE_CONTAINER, action: 'append' },
      attrs: { Id: idSignature}
    });

    return sig.getSignedXml()
}

function obtenerDatosSunat(){

    const $date = new Date()
    const fecha = {
        "ano": $date.getFullYear().toString(), 
        "mes": $date.getMonth() + 1,
        "dia": $date.getDate(),
        "hora": $date.getHours().toString().padStart(2,0),
        "min": $date.getMinutes().toString().padStart(2,0),
        "sec": $date.getSeconds().toString().padStart(2,0)
    }

    const sunat = {
        "version_ubl": {
            "documento": "2.1",
            "resumen": "2.0",
            "baja": "2.0"
        },
        "customization_id": {
            "documento": "2.0",
            "resumen": "1.0",
            "bajas": "1.0"            
        },
        "fecha": fecha.ano + "-" + fecha.mes.toString().padStart(2,0) + "-" + fecha.dia.toString().padStart(2,0),       
        "hora": fecha.hora + ":" + fecha.min + ":" + fecha.sec,
        "factura": "01",
        "boleta": "03",
        "nota_credito": "07",
        "nota_debito": "08",
        "agencia": "PE:SUNAT" 
    }

    return sunat
}

function generarNombreXml(post){

    if(post.codigo == "CE"){
        return post.emisor.ruc +"-"+post.codigo_documento +"-" + post.serie +"-" + post.numero;
    } else if(post.codigo == "RC"){
        return post.emisor.ruc +"-RC-" + post.fecha.replace(/-/g,'') +"-" + post.numero_envio;
    } else if(post.codigo == "RA"){
        return post.emisor.ruc +"-RA-" + post.fecha.replace(/-/g,'') +"-" + post.numero_envio;
    }
    
}

function generarDocumento(req, res){

    try{

        let post = req.body;
        const sunat = obtenerDatosSunat()

        if(!post.codigo || post.codigo !== "CE"){
            throw "El documento no tiene codigo o codigo es incorrecto"
        }

        if(!(post.serie && post.numero)){
            throw "El documento no tiene serio y/o numero";
        }

        if(!(post.emisor)){
            throw "El documento no tiene datos del emisor";
        }

        if(!(post.cliente)){
            throw "El documento no tiene datos del cliente";
        }

        if(post.detalle){
            $.each(post.detalle, (item,i) => {
                if(!item.valor_venta){
                    throw "El item no tiene valor de venta";
                }
                //if(!item.impuesto){
                //    throw "El item no tiene impuestos";
                //}
            })
        } else {
            throw "El documento no tiene items";
        }

        if(post.codigo_documento == sunat.nota_credito || post.codigo_documento == sunat.nota_debito){
            if(!post.referencia){
                throw "El documento no tiene referencia"
            }
        }

        const name_file = generarNombreXml(post)
        let xml = generarXmlDocumento(post, sunat)
        //console.log(xml)    
        crearArchivo(xml, name_file)
        
        res.json({"estado":"OK", "documento":name_file,"xml": base64.encode(xml) })    

    } catch(ex) {
        res.json({"error": ex.toString()})
    }
    
}

function obtenerSignature(datos,id_documento){

    return {
        "cac:Signature": {   
            "cbc:ID":id_documento,
            "cac:SignatoryParty": {
                "cac:PartyIdentification": {
                    "cbc:ID": datos.emisor.ruc
                },
                "cac:PartyName":{
                    "cbc:Name":datos.emisor.razon_social
                }    
            },
            "cac:DigitalSignatureAttachment":{
                "cac:ExternalReference": {
                    "cbc:URI":id_documento
                }
            }
        }
    }

}

function obtenerAccountingSupplier(datos, sunat){

    return {
        "cac:AccountingSupplierParty":{
            "cac:Party":{
                "cac:PartyIdentification":{
                    "cbc:ID": {
                        "@schemeID": datos.emisor.tipo_documento, 
                        "@schemaName": "Documento de Identidad",
                        "@schemaAgencyName": sunat.agencia,
                        "@schemaURI":"urn:pe:gob:sunat:see:gem:catalogos:catalogo06",
                        "#text": datos.emisor.ruc
                    }
                },
                "cac:PartyName":{
                    "cbc:Name": datos.emisor.razon_social
                },
                "cac:PartyLegalEntity":{
                    "cbc:RegistrationName":datos.emisor.razon_social,
                    "cac:RegistrationAddress":{
                        "cbc:ID": {
                            "@schemaAgencyName": "PE:INEI",
                            "@schemaName":"Ubigeos",
                            "#text": datos.emisor.ubigeo //catalogo 13
                        },
                        "cbc:CityName": datos.emisor.ciudad,
                        "cbc:CountrySubentity": datos.emisor.ciudad,
                        "cbc:District":datos.emisor.ciudad,
                        "cac:AddressLine": {
                            "cbc:Line": datos.emisor.direccion_fiscal
                        },
                        "cac:Country": {
                            "cbc:IdentificationCode": {
                                "@listID": "ISO 3166-1",
                                "@listName": "Country",
                                "@listAgencyName": "United Nations Economic Commissions for Europe",
                                "#text": datos.emisor.pais
                            } //catalogo 04
                        },
                        "cbc:AddressTypeCode": {
                            "@listAgencyName":sunat.agencia,
                            "@listName": "Establecimientos Anexos",
                            "#text": datos.emisor.codigo_sede
                        }
                    }
                }
            }
        }
    }
}

function obtenerExtensiones(datos){

    var extensiones = []
    $.each(datos.informacion_adicional, (info,i)=>{
        if(info.flat == "texto"){
            extensiones.push({
                "sac:AdditionalProperty":{
                    "cbc:ID": info.codigo,
                    "cbc:Value": info.texto
                }     
            })
        }
        if(info.flat == "numero"){
            extensiones.push({
                "sac:AdditionalMonetaryTotal":{
                    "cbc:ID": info.codigo,
                    "cbc:PayableAmount": {"@currencyID": datos.moneda, "#text":info.importe}
                }
            })
        }
    })

    return {"ext:UBLExtensions":{
        "ext:UBLExtension":{
            "ext:ExtensionContent": extensiones
        }
    }}
}

function generarXmlDocumento(datos,sunat){

    var idSignature = datos.serie + "-" + datos.numero

    var signature = obtenerSignature(datos,idSignature)

    var accountingSupplierParty = obtenerAccountingSupplier(datos,sunat)

    var UBLExtensions = obtenerExtensiones(datos)

    var accountingCustomerParty = { 
        "cac:AccountingCustomerParty":{
            "cac:Party":{
                "cac:PartyIdentification":{
                    "cbc:ID": {
                        "@schemeID":datos.cliente.tipo_documento,
                        "@schemeName":"Documento de Identidad",
                        "@schemeAgencyName": sunat.agencia,
                        "@schemeURI": "urn:pe:gob:sunat:cpe:see:catalogos:catalogo06",
                        "#text":datos.cliente.numero_documento //catalogo 06
                    }
                },
                "cac:PartyLegalEntity":{
                    "cbc:RegistrationName":datos.cliente.razon_social
                }
                //los datos de la direccion del cliente  no son obligatorios
                //otros participanes de la compra/venta no son obligatorios
            }
        }        
    }

    var totales = [];
    $.each(datos.impuestos,(total,i)=>{    
        totales.push({
            "cac:TaxTotal":{
                "cbc:TaxAmount": {"@currencyID":datos.moneda,"#text":total.importe},
                "cac:TaxSubtotal":{
                    "cbc:TaxableAmount": {"@currencyID":datos.moneda,"#text":total.importe},
                    "cbc:TaxAmount": {"@currencyID":datos.moneda,"#text":total.importe},
                    "cbc:Percent": total.porcentaje,
                    "cac:TaxCategory":{
                        "cac:TaxScheme":{
                            "cbc:ID": total.codigo,
                            "cbc:Name": total.descripcion,
                            "cbc:TaxTypeCode":total.tipo_codigo
                        }
                    }
                }
            }
        })        
    })
    
    //documentos relacionados
    //Guias

    var legalMonetaryTotal = {
        "cac:LegalMonetaryTotal":{
            "cbc:LineExtensionAmount":{"@currencyID":datos.moneda,"#text":datos.monto_total},
            "cbc:TaxInclusiveAmount":{"@currencyID":datos.moneda,"#text":datos.monto_total},
            "cbc:PayableAmount":{"@currencyID":datos.moneda,"#text":datos.monto_total}
        }
    }

    var items = []
    $.each(datos.detalle,(detalle,i)=>{
        
        items.push({
            "cbc:ID":detalle.item,
            "cbc:InvoicedQuantity":{
                "@unitCode":detalle.unidad,
                "@unitCodeListID": "UN/ECE rec 20",
                "@unitCodeListAgencyName": "United Nations Economic for Europe",
                "#text":detalle.cantidad
            }, //catalogo 03
            "cac:Item":{
                "cac:SellersItemIdentification":{
                    "cbc:ID":detalle.codigo_producto
                },
                "cac:CommodityClassification":{
                    "cbc:ItemClassificationCode": {
                        "@listID":"UNSPSC",
                        "@listAgencyName":"GS1 US",
                        "@listName": "Item Classification",
                        "#text":detalle.codigo_sunat
                    }
                },
                "cbc:Description":detalle.descripcion
            },
            "cac:Price": {
                "cbc:PriceAmount": {
                    "@currencyID":datos.moneda, 
                    "#text":detalle.precio_unitario
                }
            },    
            "cbc:LineExtensionAmount": {
                "@currencyID":datos.moneda,
                "#text":detalle.precio_unitario
            },
            "cac:PricingReference":{
                "cac:AlternativeConditionPrice":{
                    "cbc:PriceAmount": {
                        "@currencyID":datos.moneda,
                        "#text":detalle.precio_unitario
                    },
                    "cbc:PriceTypeCode": {
                        "@listName": "Tipo de Precio",
                        "@listAgencyName": sunat.agencia,
                        "@listURI":"pe:gob:cpe:see:gem:catalogos:catalogo16",
                        "#text":detalle.tipo_precio
                    }
                }           
            },
            "cac:TaxTotal":{
                "cbc:TaxAmount": {
                    "@currencyID":datos.moneda,
                    "#text":detalle.monto_total
                },
                "cac:TaxSubtotal":{
                    "cbc:TaxableAmount": {
                        "@currencyID":datos.moneda,
                        "#text":detalle.monto_base
                    },
                    "cbc:TaxAmount": {
                        "@currencyID":datos.moneda,
                        "#text":detalle.monto_tributo
                    },
                    "cac:TaxCategory":{
                        "cbc:Percent": detalle.impuesto.porcentaje,
                        "cbc:TaxExemptionReasonCode": detalle.impuesto.codigo,
                        "cac:TaxScheme":{
                            "cbc:ID": detalle.impuesto.id,
                            "cbc:Name": detalle.impuesto.name,
                            "cbc:TaxTypeCode": detalle.impuesto.codigo
                        }
                    }
                }    
            }
        })
    })
        
    if(datos.codigo_documento == sunat.nota_credito || datos.codigo_documento == sunat.nota_debito){
        const tipo_nota = datos.codigo_documento == sunat.nota_credito ? "CreditNote":"DebitNote"
        var xml = builder.create(tipo_nota) 
            .att('xmlns', 'urn:oasis:names:specification:ubl:schema:xsd:'+tipo_nota+'-2')
            .att('xmlns:sac', 'urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1')
            .att('xmlns:cac', 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2')
            .att('xmlns:cbc', 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2')
            .att('xmlns:ccts', 'urn:un:unece:uncefact:documentation:2')
            .att('xmlns:ds', 'http://www.w3.org/2000/09/xmldsig#')
            .att('xmlns:ext', 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2')
            .att('xmlns:qdt', 'urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2')
            .att('xmlns:udt', 'urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2')
            .att('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
            .ele(UBLExtensions).up()
            .ele("cbc:UBLVersionID", sunat.version_ubl.documento).up()
            .ele("cbc:CustomizationID",{"schemaAgencyName":sunat.agencia},sunat.customization_id.documento).up()
            .ele("cbc:ID", datos.serie + "-" + datos.numero).up()
            .ele("cbc:issueDate", sunat.fecha).up()
            .ele("cbc:IssueTime", sunat.hora).up()
            .ele("cbc:DueDate", datos.fecha_registro).up()
            .ele('cbc:DocumentCurrencyCode', datos.moneda).up()
            .ele('cac:DiscrepancyResponse')
                .ele('cbc:ResponseCode', datos.referencia.motivo.codigo).up()
                .ele('cbc:Description', datos.referencia.motivo.descripcion).up()
            .up()
            .ele('cac:BillingReference')
                .ele('cac:InvoiceDocumentReference')
                .ele('cbc:ID', datos.referencia.serie + '-' + datos.referencia.numero).up()
                .ele('cbc:DocumentTypeCode', datos.referencia.codigo_documento).up()
                .up()
            .up()
            .ele("cbc:DocumentCurrencyCode",datos.moneda).up()
            .ele(signature).up()
            .ele(accountingSupplierParty).up()
            .ele(accountingCustomerParty).up()

        $.each(totales,(taxTotal,i) => {
            xml.ele(taxTotal).up()
        })    

        if(datos.codigo_documento == sunat.nota_debito){
            xml.ele("cac:RequestedMonetaryTotal")
                    .ele("cbc:PayableAmount", {"currencyID": datos.moneda}, datos.monto_total.toFixed(2)).up()
                .up() 
            
        }

        xml.ele(legalMonetaryTotal).up()

        $.each(items,(invoiceLine,i)=>{
            xml.ele("cac:InvoiceLine").
                    ele(invoiceLine).up()
                .up()
        })

    } else {

        var xml = builder.create("Invoice") 
            .att('xmlns', 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2')
            .att('xmlns:sac', 'urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1')
            .att('xmlns:cac', 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2')
            .att('xmlns:cbc', 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2')
            .att('xmlns:ccts', 'urn:un:unece:uncefact:documentation:2')
            .att('xmlns:ds', 'http://www.w3.org/2000/09/xmldsig#')
            .att('xmlns:ext', 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2')
            .att('xmlns:qdt', 'urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2')
            .att('xmlns:udt', 'urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2')
            .att('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
            .ele(UBLExtensions).up()
            .ele("cbc:UBLVersionID", sunat.version_ubl.documento).up()
            .ele("cbc:CustomizationID",{"schemaAgencyName":sunat.agencia},sunat.customization_id.documento).up()
            .ele("cbc:ID", datos.serie + "-" + datos.numero).up()
            .ele("cbc:issueDate", sunat.fecha).up()
            .ele("cbc:IssueTime", sunat.hora).up()
            .ele("cbc:DueDate", datos.fecha_registro).up()
            .ele("cbc:InvoiceTypeCode", {
                "listID": datos.tipo_venta.codigo, 
                "listAgencyName":sunat.agencia,
                "listName":"Tipo de Documento",
                "listURI": "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01"}, datos.tipo_venta.valor).up() //catalogo 01
            .ele("cbc:DocumentCurrencyCode",{
                "listID":"ISO 4217 Alpha",
                "listName": "Currency",
                "listAgencyName":"United Nations Economic Commission for Europe"},datos.moneda).up()
            .ele("cbc:DueDate",datos.fecha_vencimiento)    
            .ele(signature).up()
            .ele(accountingSupplierParty).up()
            .ele(accountingCustomerParty).up()

        $.each(totales,(taxTotal,i) => {
            xml.ele(taxTotal).up()
        })    

        if(datos.gratuito == 1){
            xml.ele('cac:LegalMonetaryTotal')
                    .ele('cbc:PayableAmount', {"currencyID": datos.moneda}, datos.monto_gratuito.toFixed(2)).up()
                .up()
        }

        xml.ele(legalMonetaryTotal).up()

        $.each(items,(invoiceLine,i)=>{
            xml.ele("cac:InvoiceLine").
                    ele(invoiceLine).up()
                .up()
        })
    }

    return firmarDocumento(xml,idSignature)    
}

function generarXmlResumen(datos, sunat){

    var idSignature = "RC-"+datos.fecha.replace(/-/g,'') +"-"+datos.numero_envio

    var signature = obtenerSignature(datos,idSignature)

    var accountingSupplierParty = obtenerAccountingSupplier(datos, sunat)

    var xml = builder.create("SummaryDocuments", {version: '1.0', encoding: 'UTF-8', standalone: false})
        .att('xmlns', 'urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1')
        .att('xmlns:cac', 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2')
        .att('xmlns:cbc', 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2')
        .att('xmlns:ext', 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2')
        .att('xmlns:sac', 'urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1')
        .att('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        .ele('ext:UBLExtensions')
            .ele('ext:UBLExtension')
                .ele('ext:ExtensionContent').up()
            .up()
        .up()
        .ele("cbc:UBLVersionID",sunat.version_ubl.resumen).up()
        .ele("cbc:CustomizationID",sunat.customization_id.resumen).up()
        .ele("cbc:ID",idSignature).up()
        .ele("cbc:ReferenceDate",sunat.fecha).up()
        .ele("cbc:IssueDate",sunat.hora).up()
        .ele(signature).up()
        .ele(accountingSupplierParty).up()
    
    $.each(datos.documentos, (documento,i)=>{

        var objBillingPayment = []
        $.each(documento.montos, (payment,i)=>{
            objBillingPayment.push({
                'cbc:PaidAmount':{"@currencyID":datos.moneda,"#text":payment.importe.toFixed(2)},
                'cbc:InstructionID':payment.codigo})
        })

        var objTaxTotal = []
        $.each(documento.impuesto,(impuesto,i)=>{
            objTaxTotal.push({
                'cbc:TaxAmount':{'@currencyID':datos.moneda,'#text':impuesto.importe},
                'cac:TaxSubtotal':{
                    'cbc:TaxAmount':{'@currencyID':datos.moneda,'#text':impuesto.importe},
                    'cac:TaxCategory':{
                        'cac:TaxScheme':{'cbc:ID':impuesto.id,'cbc:Name':impuesto.descripcion,'cbc:TaxTypeCode':impuesto.codigo}
                    }
                }
            })
        })

        var objAccountingCustomerParty = {
            'cbc:CustomerAssignedAccountID':documento.cliente.numero_documento,
            'cbc:AdditionalAccountID':documento.cliente.tipo_documento
        }

        xml.ele( obtenerItemResumen(documento, sunat, objAccountingCustomerParty, objTaxTotal, objBillingPayment) ).up()
    })
    
    //return xml.end({pretty: true})
    return firmarDocumento(xml,idSignature)
}

function obtenerItemResumen(documento, sunat, objAccountingCustomerParty, objTaxTotal, objBillingPayment){

    if(documento.tipo_documento == sunat.nota_credito || documento.tipo_documento == sunat.nota_debito){
        return {
            'cbc:LineID': documento.item,
            'cbc:DocumentTypeCode':documento.tipo_documento,
            'cbc:ID': documento.serie + '-' + documento.numero,
            'cac:AccountingCustomerParty': objAccountingCustomerParty,
            'cac:InvoiceDocumentReference': {
                'cbc:ID':documento.referencia.serie,
                'cbc:DocumentTypeCode':documento.referencia.numero
            },
            'cac:Status': {'cbc:ConditionCode':documento.estado},
            'sac:TotalAmount': {"@currencyID":documento.moneda,"#text":documento.monto_total.toFixed(2)},
            'sac:BillingPayment': objBillingPayment,
            'cac:TaxTotal': objTaxTotal,
        }     
    }

    return {
        'cbc:LineID': documento.item,
        'cbc:DocumentTypeCode':documento.tipo_documento,
        'cbc:ID': documento.serie + '-' + documento.numero,
        'cac:AccountingCustomerParty': objAccountingCustomerParty,
        'cac:Status': {'cbc:ConditionCode':documento.estado},
        'sac:TotalAmount': {"@currencyID":documento.moneda,"#text":documento.monto_total.toFixed(2)},
        'sac:BillingPayment': objBillingPayment,
        'cac:TaxTotal': objTaxTotal,
    }
}

function generarResumen (req, res)  {

    try{

        let post = req.body;
        const sunat = obtenerDatosSunat()

        if(!post.codigo || post.codigo !== "RC"){
            throw "El resumen no tiene codigo o codigo es incorrecto"
        }

        if(!(post.fecha && post.numero_envio)){
            throw "El resumen no tiene fecha y/o numero de envio";
        }

        if(!(post.emisor)){
            throw "El resumen no tiene datos del emisor";
        }

        if(post.documentos){
            $.each(post.documentos, (item,i) => {

                if(!(item.cliente)){
                    throw "El item no tiene datos del cliente";
                }

                if(!item.monto_total){
                    throw "El item no tiene valor de venta";
                }
                if(!item.montos){
                    throw "El item no tiene el detalle de montos";
                }

                if(item.tipo_documento == sunat.nota_credito || item.tipo_documento == sunat.nota_debito){
                    if(!item.referencia){
                        throw "El resumen no tiene referencia"
                    }
                }

            })
        } else {
            throw "El resumen no tiene documentos";
        }

        const name_file = generarNombreXml(post)
        let xml = generarXmlResumen(post,sunat)
            
        crearArchivo(xml, name_file)

        res.json({"estado":"OK", "documento":name_file,"xml": base64.encode(xml) }) 
        
    } catch(ex){
        res.json({"error": ex.toString()})
    }
}

function generarXmlBaja(datos,sunat){

    var idSignature = datos.codigo + "-" + datos.fecha.replace(/-/g,'') + "-" + datos.numero_envio

    var signature = obtenerSignature(datos,idSignature)

    var accountingSupplierParty = obtenerAccountingSupplier(datos,sunat)

    var xml = builder.create("VoidedDocuments",{version: '1.0', encoding: 'UTF-8', standalone: false})
        .att('xmlns', 'urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1')
        .att('xmlns:cac', 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2')
        .att('xmlns:cbc', 'urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2')
        .att('xmlns:ext', 'urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2')
        .att('xmlns:sac', 'urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1')
        .att('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        .ele('ext:UBLExtensions')
            .ele('ext:UBLExtension')
                .ele('ext:ExtensionContent').up()
            .up()
        .up()
        .ele("cbc:UBLVersionID",sunat.version_ubl.baja).up()
        .ele("cbc:CustomizationID",sunat.customization_id.baja).up()
        .ele("cbc:ID",idSignature).up()
        .ele("cbc:ReferenceDate",sunat.fecha).up()
        .ele("cbc:IssueDate",sunat.hora).up()
        .ele(signature).up()
        .ele(accountingSupplierParty).up()

    var items = []
    $.each(datos.documentos, (documento, i) => {
        items.push({
            "cbc:LineID": documento.linea,
            "cbc:DocumentTypeCode": documento.tipo_documento,
            "sac:DocumentSerialID": documento.serie,
            "sac:DocumentNumberID": documento.numero,
            "sac:VoidReasonDescription": documento.descripcion
        })
    })

    var voiceDocumentsLine = {"sac:VoidedDocumentsLine": items}
    
    xml.ele(voiceDocumentsLine).up();        

    return firmarDocumento(xml,idSignature)
}

function generarBaja (req, res)  {
    
    try{

        let post = req.body
        const sunat = obtenerDatosSunat()

        if(!post.codigo || post.codigo !== "RA"){
            throw "La comunicacion de baja no tiene codigo o codigo es incorrecto"
        }

        if(!(post.fecha && post.numero_envio)){
            throw "La comunicacion de baja no tiene fecha y/o numero de envio";
        }

        if(!(post.emisor)){
            throw "La comunicacion de baja no tiene datos del emisor";
        }

        if(!post.documentos){
            throw "La comunicacion de baja no tiene documentos"
        }

        const name_file = generarNombreXml(post)
        let xml = generarXmlBaja(post,sunat)    
            
        crearArchivo(xml, name_file)
        
        res.json({"estado": "OK", "documento":name_file, "xml": base64.encode(xml) })    

    } catch(ex) {
        res.json({"error": ex.toString()})
    }
}
     
// Exportamos las funciones en un objeto json para poder usarlas en otros fuera de este fichero
module.exports = {
    generarDocumento, generarResumen, generarBaja
};
    