const { Router } = require('express')
const router = Router()
const $ = require('underscore')

router.get('/', (req, res) => {
    res.json({"error": "ruta incorrecta"})
})

const electronico = require('../../controllers/electronicos');

router.post('/generar-documento',electronico.generarDocumento);
router.post('/generar-resumen',electronico.generarResumen);
router.post('/generar-baja',electronico.generarBaja);

router.get('/obtener-documentos', (req, res) => {

    const documentos = require('../../test/documentos.json')
    let docs = [] 
    $.each(documentos, (documento,i) => {
        docs.push(documento.name)    
    })
    res.json(docs)
})

router.get('/index',(req,res)=>{
    //res.render("<p>hola mundo</p>")
})

module.exports = router;