const express = require('express')
const app  = express()
const morgan = require('morgan')

//configuracion
app.set('port', process.env.PORT || 3000)
app.set('json spaces',2)

/*midlewords*/ 
app.use(morgan('dev')) 
//app.use(morgan('combined'))

//reconocer formato json
app.use(express.json())

//reconocer formularios (inputs)
app.use(express.urlencoded({extended: false}))

/*router*/
app.use('/api/',require('./src/routes/router'))

app.listen( app.get('port'), () => {
    console.log('Server ejecutandose por el puerto ' + app.get('port'))
} )